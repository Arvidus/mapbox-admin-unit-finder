import { combineReducers } from 'redux';

const payloadExists = (state, payload) => {
    return state.find((s) => s.name === payload.name);
};

const provinceReducer = (state = [], action) => {
    switch (action.type) {
        case "FETCH_PROVINCE_ARMS":
            return action.payload;
        default:
            return state
    }
};
const countyReducer = (state = [], action) => {
       switch (action.type) {
           case "FETCH_COUNTY_ARMS":
               return action.payload;
           default:
               return state
       }
};
const municipalityReducer = (state = [], action) => {
    switch (action.type) {
        case "FETCH_MUNICIPAL_ARMS":
            return action.payload;
        default:
            return state
    }
};
const coordinatesReducer = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_COORDINATES":
            return action.payload;
        default:
            return state
    }
};
const unitsReducer = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_UNITS":
            return action.payload;
        default:
            return state
    }
};
const visitsReducer = (state = {
    provinces: [],
    counties: [],
    municipalities: []
}, action) => {
    switch (action.type) {
        case "FETCH_PROVINCES":
            if (payloadExists(state.provinces, action.payload)){
                return state
            } else {
                return {...state, provinces: [...state.provinces, action.payload]};
            }
        case "FETCH_COUNTIES":
            if (payloadExists(state.counties, action.payload)){
                return state
            } else {
                return {...state, counties: [...state.counties, action.payload]};
            }
        case "FETCH_MUNICIPALITIES":
            if (payloadExists(state.municipalities, action.payload)){
                return state
            } else {
                return {...state, municipalities: [...state.municipalities, action.payload]};
            }
        default:
            return state
    }
};

export default combineReducers({
    provinces: provinceReducer,
    counties: countyReducer,
    municipalities: municipalityReducer,
    coordinates: coordinatesReducer,
    units: unitsReducer,
    visits: visitsReducer
});
