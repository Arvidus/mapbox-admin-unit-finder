import wikipedia from "../../api/wikipedia.api";

const getData = (title) => {
    console.log(title);
    return wikipedia.get("api.php", {
        params: {
            format: "json",
            formatversion: 2,
            action: "query",
            prop: "images",
            titles: title,
            imlimit: 500,
            pltitles: null,
            inprop: "url",
            origin: "*"
        }
    });
};

const buildUrlArray = function (response, unitType) {

    let imageUrlArray = [];

    console.log(response);

    if (response.data.query.pages[0].images) {
        response.data.query.pages[0].images.forEach(function (page) {


            if (page.title.split('Fil:')[1].includes("Halmstad")) {
                imageUrlArray = [...imageUrlArray, {
                    url: "https://commons.wikimedia.org/wiki/Special:FilePath/File:Halmstad_vapen.svg?width=50",
                    name: "Halmstad",
                    unitType
                }]
            } else if (page.title.split('Fil:')[1].includes("Landskrona")) {
                imageUrlArray = [...imageUrlArray, {
                    url: "https://commons.wikimedia.org/wiki/Special:FilePath/File:Landskrona_vapen.svg?width=50",
                    name: "Landskrona",
                    unitType
                }]
            } else if (page.title.split('Fil:')[1].includes("Upplands_Väsby")) {
                console.log(page.title);
                imageUrlArray = [...imageUrlArray, {
                    url: "https://commons.wikimedia.org/wiki/Special:FilePath/File:Upplands_Väsby_vapen.svg?width=50",
                    name: "Upplands Väsby",
                    unitType
                }]
            } else if (page.title.split('Fil:')[1].includes("Skoevde")) {
                console.log(page.title);
                imageUrlArray = [...imageUrlArray, {
                    url: "https://commons.wikimedia.org/wiki/Special:FilePath/File:Skoevde_vapen.svg?width=50",
                    name: "Skövde",
                    unitType
                }]
            } else if (page.title.split('Fil:')[1].includes("Uppland") && unitType === "County") {
                console.log(page.title);
                imageUrlArray = [...imageUrlArray, {
                    url: "https://commons.wikimedia.org/wiki/Special:FilePath/File:Upplands_vapen.svg?width=50",
                    name: "Uppsala",
                    unitType
                }]
            } else if (page.title.toLowerCase().includes("vapen") || page.title.toLowerCase().includes("arms")) {
                imageUrlArray = [...imageUrlArray, {
                    url: "https://commons.wikimedia.org/wiki/Special:FilePath/" +
                        page.title.split(' ').join('_').split('Fil:').join('File:') + "?width=50",
                    name: page.title.split('Fil:')[1].split('s ')[0].split(" v")[0]
                        .split(" m")[0],
                    unitType
                }];
            }

        });
    }

    console.log(imageUrlArray);

    return imageUrlArray;
};

export const getArms = () => async dispatch => {

    const provinceResponse = await getData("Landskapsvapen_i_Sverige");
    const countyResponse = await getData("Galleri_över_länsvapen_i_Sverige");
    const municipalityResponse = await getData("Kommunvapen_i_Sverige");

    const provinceArray = buildUrlArray(provinceResponse, "Province");
    const countyArray = buildUrlArray(countyResponse, "County");
    const municipalityArray = buildUrlArray(municipalityResponse, "Municipality");

    dispatch({type: "FETCH_PROVINCE_ARMS", payload: provinceArray});
    dispatch({type: "FETCH_COUNTY_ARMS", payload: countyArray});
    dispatch({type: "FETCH_MUNICIPAL_ARMS", payload: municipalityArray});
};

