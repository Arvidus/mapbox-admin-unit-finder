import geoNames from "../../api/geoNames.api";
import Provinces from "../../provinces";

export const getGeoData = (latitude, longitude) => async dispatch => {

    dispatch({type: "FETCH_COORDINATES", payload: {lat: latitude, lon: longitude}});

    const userName = "arvidus";

    const response = await geoNames.get(`countrySubdivisionJSON`, {
        params: {
            lat: latitude,
            lng: longitude,
            username: userName,
            level: 2
        }
    });

    if (response.data.countryName === "Sweden") {

        console.log(response.data);

        const provinceArray = Provinces;

        let county;
        let municipality;
        let province;

        const getUnitNames = (objectType, unitType) => {
            if (objectType.includes("s " + unitType)) {
                if (objectType.includes("äs " + unitType) || objectType.includes("ås " + unitType) ||
                    objectType.includes("Grums " + unitType) || objectType.includes("fors " + unitType)) {
                    return objectType.substring(0, objectType.indexOf(" " + unitType));
                } else {
                    return objectType.substring(0, objectType.indexOf("s " + unitType));
                }
            } else if (objectType.includes(" " + unitType)) {
                return objectType.substring(0, objectType.indexOf(" " + unitType));
            } else {
                return objectType;
            }
        };

        county = getUnitNames(response.data.adminName1, "län");

        if (response.data.adminName2.includes("Kommun")) {
            municipality = getUnitNames(response.data.adminName2, "Kommun");
        } else if (response.data.adminName2.includes("kommun")) {
            municipality = getUnitNames(response.data.adminName2, "kommun");
        } else if (response.data.adminName2.includes("Municipality")) {
            municipality = getUnitNames(response.data.adminName2, "Municipality");
        }

        loop1:
            for (let key of Object.keys(provinceArray)) {
                for (let subKey of provinceArray[key]) {
                    if (subKey === municipality) {
                        console.log(key);
                        province = key;
                        break loop1;
                    }
                }
            }
        const units = {
            county,
            municipality,
            province
        };

        dispatch({type: "FETCH_UNITS", payload: units});

    } else {
        alert("Please select coordinates within Swedish territory")
    }
};

export const setVisits = (unit) => dispatch => {
    switch (unit.unitType) {
        case "Province":
            dispatch({type: "FETCH_PROVINCES", payload: unit});
            break;
        case "County":
            dispatch({type: "FETCH_COUNTIES", payload: unit});
            break;
        case "Municipality":
            dispatch({type: "FETCH_MUNICIPALITIES", payload: unit});
            break;
        default:
            return
    }
};


