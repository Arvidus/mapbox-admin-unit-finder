import React, {useEffect} from 'react';
import ReactMapboxGl from "react-mapbox-gl";
import {connect} from 'react-redux';
import {getGeoData} from "../../redux/actions/index.actions";
import {getArms} from "../../redux/actions/firstGets.actions";

import './MapBoxViewStyle.styles.css'
import InfoPopup from "./InfoPopup/InfoPopup.component";
import VisitList from "./VisitList/VisitList.component";

const MapBoxViewComponent = ({getGeoData, getArms}) => {

    const Map = ReactMapboxGl({
        container: 'Map',
        accessToken: "pk.eyJ1IjoiYXJ2aWR1cyIsImEiOiJjamFyd3I4MWQ1NWZkMzNwN3BqeG1hNGo0In0.Wtk27hioiHrm2arH3L6uUw",
        classes: ["name"]
    });

    useEffect(() => {
        getArms();
        getGeoData(59.3329927, 17.9811692);
    }, [getArms, getGeoData]);

    const setMapCoordinates = (map, evt) => {
        getGeoData(evt.lngLat.lat, evt.lngLat.lng);
    };


    return (
        <div className={"mapBoxView"}>
            <VisitList/>
            <Map
                style="mapbox://styles/mapbox/streets-v11"
                zoom={[11]}
                center={[17.9811692, 59.3329927]}
                onClick={setMapCoordinates}
                containerStyle={{
                    height: "100vh",
                    width: "100%"
                }}
            >
                <InfoPopup/>
            </Map>
        </div>
    );
};


export default connect(null, {
    getGeoData,
    getArms
})(MapBoxViewComponent);