import React from 'react';
import {connect} from "react-redux"

import "./VisitList.styles.css"

const VisitList = ({visits}) => {

    const renderVisits = () => {
        return (
            <React.Fragment>
                <div>
                    <h3>Provinces</h3>
                    {visits.provinces.map((province) => {
                        return (
                            <img key={province.name} alt={province.name} src={province.url}/>
                        )
                    })}
                </div>
                <div>
                    <h3>Counties</h3>
                    {visits.counties.map((county) => {
                        return (
                            <img key={county.name} alt={county.name} src={county.url}/>
                        )
                    })}
                </div>
                <div>
                    <h3>Municipalities</h3>
                    {visits.municipalities.map((municipality) => {
                        return (
                            <img  key={municipality.name} alt={municipality.name} src={municipality.url}/>
                        )
                    })}
                </div>
            </React.Fragment>
        )
    };

    return (
        <div className={"visitList"}>
            {renderVisits()}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        visits: state.visits
    }
};

export default connect(mapStateToProps)(VisitList)