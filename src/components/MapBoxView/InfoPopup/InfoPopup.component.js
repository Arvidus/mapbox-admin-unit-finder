import React from 'react';
import {connect, useSelector} from 'react-redux';
import {Popup} from 'react-mapbox-gl';

import UnitView from "./UnitView/UnitView.component";

import "./InfoPopup.styles.css";

const InfoPopupComponent = props => {

    const coordinates = useSelector(state => state.coordinates);

    return (
        <Popup className="infoPopup" coordinates={[coordinates.lon, coordinates.lat]}>
            <UnitView/>
        </Popup>
    );
};

export default connect(null)(InfoPopupComponent);