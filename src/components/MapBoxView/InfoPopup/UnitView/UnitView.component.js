import React from 'react';
import {connect} from 'react-redux';

import {setVisits} from "../../../../redux/actions/index.actions";

import './UnitViewStyle.styles.css';

const UnitViewComponent = ({counties, municipalities, provinces, units, setVisits}) => {

    const getUnits = (unitArray, unitName) => {

        let unitUrl;

        if (unitName) {
            for (let item of unitArray) {
                if (item.url.includes(unitName.split(' ').join('_'))) {
                    console.log(item.name);
                    console.log(item.url);
                    unitUrl = item.url;
                    setVisits(item);
                    break;
                } else if (unitName === "Skövde" && item.name === "Skövde") {
                    console.log(item.url);
                    unitUrl = item.url;
                    setVisits(item);
                    break;
                } else if (unitName === "Malung-Sälen" && item.name === "Malung") {
                    console.log(item.url);
                    unitUrl = item.url;
                    setVisits(item);
                    break;
                } else if (unitName === "Uppsala" && item.name === "Uppsala") {
                    console.log(item.url);
                    unitUrl = item.url;
                    setVisits(item);
                }
            }
            return <img alt={unitName} src={unitUrl}/>
        } else {
            return <div>Loading...</div>
        }

    };

    return (

        <div className={"unitView"}>
            <h3>Province</h3>
            <div>
                {getUnits(provinces, units.province)}
                <span>{" " + units.province}</span>
            </div>
            <h3>County</h3>
            <div>
                {getUnits(counties, units.county)}
                <span>{" " + units.county}</span>
            </div>
            <h3>Municipality</h3>
            <div>
                {getUnits(municipalities, units.municipality)}
                <span>{" " + units.municipality}</span>
            </div>
        </div>


    )
};

const mapStateToProps = (state) => {
    return {
        provinces: state.provinces,
        counties: state.counties,
        municipalities: state.municipalities,
        units: state.units
    }
};

export default connect(mapStateToProps, {
    setVisits
})(UnitViewComponent);
