import React from 'react';

import MapBoxView from "./MapBoxView/MapBoxView.component";

const App = () => {

    return (
        <div>
            <MapBoxView/>
        </div>
    );
};

export default App;