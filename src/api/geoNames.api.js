import axios from 'axios';

export default axios.create({
    baseURL: 'https://secure.geonames.org/'
})

