--- **Description** ---

A single-page application that reads coordinates and calculates in which Swedish province, county and municipality the coordinates are located.

-- **Pre-requisites** --

-   NodeJS

-- **Install** --

-   Run `npm install` in directory from command prompt or IDE terminal.

-- **How to use** --

-   Run `npm start` in directory from command prompt or IDE terminal.

-	An interactive map will render. You are able to navigate the map by holding down the left mouse button and dragging the mouse.

-	To read coordinates, click anywhere witihin Swedish sovereign territory on the map to read coordinates and display Swedish administrative units (name and administrative emblem in shape of coat of arms).

-	OBS: The application only functions with coordinates within Swedish territory. Any other country's territory will result in an alert-popup.

-- **Developer's Notes** --

-	This application was developed using JavaScript-based front end framework ReactJS along with JavaScript library Redux. MapBox Graphical Layout was used to render the interactive map. Administrative units is calculated using Nominatim API by OpenStreetMaps. Links for images of administrative coat of arms is loaded from Wikipedia.com using MediaWiki API.

-	The application is a proof of concept for future projects, which involves GPS-based and augmented reality applications using mostly mobile phones.